<?php
/**
 * Created by PhpStorm.
 * User: Dev1-2
 * Date: 8/18/2015
 * Time: 11:46 AM
 */

header("Access-Control-Allow-Origin: *");

$errors = array();      // array to hold validation errors
$data = array();      // array to pass back data

if(!function_exists("array_column"))
{
    function array_column($array,$column_name)
    {
        return array_map(function($element) use($column_name){return $element[$column_name];}, $array);
    }
}

// validate the variables ======================================================
if (empty($_POST['author']))
    $errors['author'] = 'Your email is required.';
if (empty($_POST['body']))
    $errors['body'] = 'Need some comments to show.';
if (empty($_POST['postid']))
    $errors['postid'] = 'The post id doesn\'t exists.';
// return a response ===========================================================

// response if there are errors
if (!empty($errors)) {
    // if there are items in our errors array, return those errors
    $data['success'] = false;
    $data['errors']  = $errors;
    $data['message'] = 'Sorry, some issue occur in the process, try later.';
} else {
    // if there are no errors, return a message
    $data['success'] = true;
    $data['message'] = 'Thanks for your comment.';

    $email_to = "lidiexy@fuzenetmarketing.com";
    $postid = $_POST['postid'];
    $commentid = uniqid('c_', true);
    $cretiondate = date('F d, Y \a\t h:ia');
    $approved = false;
    $author = $_POST['author']; // required
    $email_subject = "(NEED APPROVAL) The user " . $author . " comment on the Blog Post of FuzeNet." ;
    $email_from = $author; // required
    $message = $_POST['body']; // required

    $email_message = "We need approval for this comment done on the Blog of Fuzenet.\n\n";

    $email_message .= "Post ID: ".$postid."\n";
    $email_message .= "Author: ".$author."\n";
    $email_message .= "Comment: ".$message."\n";
    $email_message .= "Creation Date: ".$cretiondate."\n";

    $headers = 'From: '.$email_from."\r\n".
        'Reply-To: '.$email_from."\r\n" .
        'X-Mailer: PHP/' . phpversion();

    //Load the json and Write in it
    if($file_handler = file_get_contents('../data/blog.json')) {
        $quote_handler = json_decode($file_handler);
        $thepost = array_filter($quote_handler, function($post) {
            return $post->id == $_POST['postid'];
        });
        $key = key($thepost);
        array_push($quote_handler[$key]->comments, array(
            "comment_id"=> $commentid,
            "comment_author"=> $author,
            "comment_body"=> $message,
            "comment_creationdate"=> $cretiondate,
            "comment_approved" => false
        ));
        $jsonSaveData = json_encode($quote_handler, JSON_UNESCAPED_UNICODE);
        if(file_put_contents('../data/blog.json', stripcslashes($jsonSaveData))) {
            if(!@mail($email_to, $email_subject, $email_message, $headers)){
                $data['success'] = false;
                $data['message'] = 'Error sending approval request for your comment to FuzeNet Team.';
            }
        } else {
            $data['success'] = false;
            $data['message'] = 'Error writing the comment in the blog data.';
        }
    } else {
        $data['success'] = false;
        $data['message'] = 'Error reading the post information.';
    }
}

// return all our data to an AJAX call
echo json_encode($data);