<?php
/**
 * Created by PhpStorm.
 * User: Lidiexy
 * Date: 8/12/2015
 * Time: 12:47 PM
 */
header("Access-Control-Allow-Origin: *");
//Send the email to FuzeNet Marketing Team

$errors = array();      // array to hold validation errors
$data = array();      // array to pass back data

// validate the variables ======================================================
if (empty($_POST['name']))
    $errors['name'] = 'Name is required.';
if (empty($_POST['email']))
    $errors['email'] = 'Email is required.';
if (empty($_POST['msg']))
    $errors['msg'] = 'Message is required.';
// return a response ===========================================================

// response if there are errors
if (!empty($errors)) {
    // if there are items in our errors array, return those errors
    $data['success'] = false;
    $data['errors']  = $errors;
    $data['message'] = 'Sorry, some issue occur in the process, try later.';
} else {
    // if there are no errors, return a message
    $data['success'] = true;
    $data['message'] = 'Thanks for your message, we will get back to you soon';

    $email_to = "lidiexy@fuzenetmarketing.com";
    $name = $_POST['name']; // required
    $email_subject = "The user " . $name . " wants to get in contact with FuzeNet." ;
    $email_from = $_POST['email']; // required
    $message = $_POST['msg']; // required

    $email_message = "Form details below.\n\n";

    $email_message .= "Name: ".$name."\n";
    $email_message .= "Email: ".$email_from."\n";
    $email_message .= "Message: ".$message."\n";

    $headers = 'From: '.$email_from."\r\n".
        'Reply-To: '.$email_from."\r\n" .
        'X-Mailer: PHP/' . phpversion();

    //Load the json and Write in it
    if($file_handler = file_get_contents('../data/contact.json')) {
        $contact_handler = json_decode($file_handler);
        array_push($contact_handler, array(
            "name"=> $name,
            "email"=> $email_from,
            "msg" => $message,
            "created_at" => date('m-d-Y h:i:s')
        ));
        $jsonSaveData = json_encode($contact_handler);
        if(file_put_contents('../data/contact.json', $jsonSaveData)) {
            if(!@mail($email_to, $email_subject, $email_message, $headers)){
                $data['success'] = false;
                $data['message'] = 'Error sending your email to FuzeNet Team.';
            }
        } else {
            $data['success'] = false;
            $data['message'] = 'Error writing the contact information.';
        }
    } else {
        $data['success'] = false;
        $data['message'] = 'Error reading the contact information.';
    }
}

// return all our data to an AJAX call
echo json_encode($data);