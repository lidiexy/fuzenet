<?php
/**
 * Created by PhpStorm.
 * User: Lidiexy
 * Date: 8/12/2015
 * Time: 12:47 PM
 */
header("Access-Control-Allow-Origin: *");
//Send the email to FuzeNet Marketing Team

$errors = array();      // array to hold validation errors
$data = array();      // array to pass back data

// validate the variables ======================================================
if (empty($_POST['qemail']))
    $errors['qemail'] = 'Email is required.';
// return a response ===========================================================

// response if there are errors
if (!empty($errors)) {
    // if there are items in our errors array, return those errors
    $data['success'] = false;
    $data['errors']  = $errors;
    $data['message'] = 'Sorry, some issue occur in the process, try later.';
} else {
    // if there are no errors, return a message
    $data['success'] = true;
    $data['message'] = 'Thanks for your quote, we will get back to you soon';

    $email_to = "lidiexy@fuzenetmarketing.com";
    $email_from = $_POST['qemail']; // required
    $email_subject = "The user " . $email_from . " wants to get a quote with us." ;
    $message = 'We need to add this email to our customer'; // required
    $creation_date = $_POST['createdq_at'];

    $email_message = "Detail of a quote below.\n\n";

    $email_message .= "Email: ".$email_from."\n";
    $email_message .= "Creation Date: ".$creation_date."\n";

    $headers = 'From: '.$email_from."\r\n".
        'Reply-To: '.$email_from."\r\n" .
        'X-Mailer: PHP/' . phpversion();

    //Load the json and Write in it
    if($file_handler = file_get_contents('../data/quote.json')) {
        $quote_handler = json_decode($file_handler);
        array_push($quote_handler, array(
            "email"=> $email_from,
            "createdq_at" => date('m-d-Y h:i:s')
        ));
        $jsonSaveData = json_encode($quote_handler);
        if(file_put_contents('../data/quote.json', $jsonSaveData)) {
            if(!@mail($email_to, $email_subject, $email_message, $headers)){
                $data['success'] = false;
                $data['message'] = 'Error sending your email to FuzeNet Team.';
            }
        } else {
            $data['success'] = false;
            $data['message'] = 'Error writing the quote information.';
        }
    } else {
        $data['success'] = false;
        $data['message'] = 'Error reading the quote information.';
    }
}

// return all our data to an AJAX call
echo json_encode($data);