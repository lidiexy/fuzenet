(function() {
  'use strict';

  angular
    .module('fuzenetv1', ['ngAnimate', 'ngCookies', 'ngTouch', 'ngSanitize', 'restangular', 'ui.router', 'wu.masonry']);

})();
