(function() {
  'use strict';

  angular
    .module('fuzenetv1')
    .directive('acmeFooter', acmeFooter);

  /** @ngInject */
  function acmeFooter() {
    var directive = {
      restrict: 'E',
      templateUrl: 'app/components/footer/footer.html',
      controller: FooterController,
      controllerAs: 'ct',
      bindToController: true
    };

    return directive;

    /** @ngInject */
    function FooterController($http, toastr) {
      $http.defaults.useXDomain = true;
      $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";

      var vm = this;
      vm.formData = {
        name: '',
        email: '',
        msg: '',
        created_at: ''
      };

      vm.contactSubmit = function() {
        vm.formData.created_at = new Date();
        $http({
          method  : 'POST',
          url     : 'http://fuzenet.local/src/backend/contact.php',
          data    : $.param(vm.formData),  // pass in data as strings
          headers : { 'Content-Type': 'application/x-www-form-urlencoded' } // set the headers so angular passing info as form data (not request payload)
        })
        .success(function(data) {
          if (!data.success) {
            toastr.error(data.message);
          } else {
            //Before erase the formData make a copy of all the fields in the contact.json
            vm.formData = {};
            toastr.success(data.message);
          }
        });
        return false;
      }
    }
  }

})();
