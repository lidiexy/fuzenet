/*(function() {
  'use strict';

  angular
    .module('fuzenetv1')
    .directive('masonry', masonry);

  /!** @ngInject *!/
  function masonry() {
    var NGREPEAT_SOURCE_RE = '<!-- ngRepeat: ((.*) in ((.*?)( track by (.*))?)) -->';
    return {
      compile: function(element, attrs) {
        // auto add animation to brick element
        var animation = attrs.ngAnimate || "'masonry'";
        var $brick = element.children();
        $brick.attr("ng-animate", animation);

        // generate item selector (exclude leaving items)
        var type = $brick.prop('tagName');
        var itemSelector = type+":not([class$='-leave-active'])";

        return function (scope, element, attrs) {
          var options = angular.extend({
            itemSelector: itemSelector
          }, scope.$eval(attrs.masonry));

          // try to infer model from ngRepeat
          if (!options.model) {
            var ngRepeatMatch = element.html().match(NGREPEAT_SOURCE_RE);
            if (ngRepeatMatch) {
              options.model = ngRepeatMatch[4];
            }
          }

          // initial animation
          element.addClass('masonry');

          // Wait inside directives to render
          setTimeout(function () {
            element.masonry(options);

            element.on("$destroy", function () {
              element.masonry('destroy')
            });

            if (options.model) {
              scope.$apply(function() {
                scope.$watchCollection(options.model, function (_new, _old) {
                  if(_new == _old) return;

                  // Wait inside directives to render
                  setTimeout(function () {
                    element.masonry("reload");
                  });
                });
              });
            }
          });
        };
      }
    };
  }

})();*/

/**
 *
 * EXAMPLE OF USE WITH ANGULAR-MASONRY:
 * <input type="text" ng-model="main.nameFilter" />
 * <a class="btn btn-default" ng-click="main.order = 'id'">Order by id</a>
 * <a class="btn btn-default" ng-click="main.order = 'name'">Order by name</a>
 * <a class="btn btn-default" ng-click="main.order = 'date'">Order by date</a>
 * <a class="btn btn-default" ng-click="main.subcategoryFilter = 'logo_design'"> Logos </a>
 * <a class="btn btn-default" ng-click="main.subcategoryFilter = ''"> Show All </a>
 *
 * <div masonry="true">
 *     <article class="masonry-brick col-xs-12 col-sm-6 col-md-4" ng-repeat="item in main.portfolio.design | filter: { name: main.nameFilter } | filter: {subcategory: main.subcategoryFilter} | orderBy: main.order">
 *         <figure class="item-portfolio-hold">
 *             <img ng-src="{{item.picture}}" class="img-responsive"/>
 *             <figcaption class="item-portfolio-info">
 *                 <h4>{{item.name}} <br/> <small>{{item.tagline}}</small> </h4>
 *             </figcaption>
 *         </figure>
 *     </article>
 * </div>
 *
 */

/**
 * EXAMPLU WITH MASONRY DIRECTIVE ALONE
 * <ul class="list-inline grid" masonry>
 <li class="col-xs-12 col-sm-6 col-md-4 masonry-brick" ng-repeat="item in main.portfolio.design | filter: { name: main.nameFilter } | filter: {subcategory: main.subcategoryFilter} | orderBy: main.order">
 <article class="item-portfolio">
 <figure class="item-portfolio-hold">
 <img ng-src="{{item.picture}}" class="img-responsive"/>
 <figcaption class="item-portfolio-info">
 <h4>{{item.name}} <br/> <small>{{item.tagline}}</small> </h4>
 </figcaption>
 </figure>
 </article>
 </li>
 </ul>
 */
