(function () {
    'use strict';

    angular
        .module('fuzenetv1')
        .service('webDevTec', webDevTec);

    /** @ngInject */
    function webDevTec($http) {
        var team = [
            {
                'name': 'Keith Troup',
                'position': 'Head Manager',
                'url': 'https://keith.fuzenetmarketing.com/',
                'brief': '',
                'social': {
                    'linkedin': '',
                    'googleplus': 'https://keith.fuzenetmarketing.com/',
                    'twitter': '',
                    'facebook': ''
                },
                'picture': 'https://lh5.googleusercontent.com/-Q_aJquMgEwU/AAAAAAAAAAI/AAAAAAAAABg/-TQ-ptoyKMI/photo.jpg?sz=320'
            },
            {
                'name': 'Randy Hinger',
                'position': 'Content Manager',
                'url': 'https://randy.fuzenetmarketing.com/',
                'brief': '',
                'social': {
                    'linkedin': '',
                    'googleplus': 'https://randy.fuzenetmarketing.com/',
                    'twitter': '',
                    'facebook': ''
                },
                'picture': 'https://lh5.googleusercontent.com/-AIwLp_UDAYU/AAAAAAAAAAI/AAAAAAAAAwo/Iy48gSOJFbE/photo.jpg?sz=320'
            },
            {
                'name': 'Ken Fox',
                'position': 'SEO Engineer',
                'url': 'https://ken.fuzenetmarketing.com/',
                'brief': '',
                'social': {
                    'linkedin': '',
                    'googleplus': 'https://ken.fuzenetmarketing.com/',
                    'twitter': '',
                    'facebook': ''
                },
                'picture': 'https://lh6.googleusercontent.com/-L94uhKwCcXA/AAAAAAAAAAI/AAAAAAAAATg/f49KQn-5yJs/photo.jpg?sz=320'
            },
            {
                'name': 'Autumn Day',
                'position': 'Creative Designer',
                'url': 'https://autumn.fuzenetmarketing.com/',
                'brief': '',
                'social': {
                    'linkedin': '',
                    'googleplus': 'https://autumn.fuzenetmarketing.com/',
                    'twitter': '',
                    'facebook': ''
                },
                'picture': 'https://lh5.googleusercontent.com/-sIuFsjINzxs/AAAAAAAAAAI/AAAAAAAAAXw/_lz6rqx6HoY/photo.jpg?sz=320'
            },
            {
                'name': 'Pejmon Hosseinzadeh',
                'position': 'Creative Designer',
                'url': 'https://pejmon.fuzenetmarketing.com/',
                'brief': '',
                'social': {
                    'linkedin': '',
                    'googleplus': 'https://pejmon.fuzenetmarketing.com/',
                    'twitter': '',
                    'facebook': ''
                },
                'picture': 'https://lh6.googleusercontent.com/-yjSPDQW1zc8/AAAAAAAAAAI/AAAAAAAAAEI/zWchlg8Nkto/photo.jpg?sz=320'
            },
            {
                'name': 'Lidiexy Alonso',
                'position': 'Developer & Designer',
                'url': 'https://lidiexy.fuzenetmarketing.com/',
                'brief': 'Passionate about pushing boundaries and building amazing apps.',
                'social': {
                    'linkedin': '',
                    'googleplus': 'https://lidiexy.fuzenetmarketing.com/',
                    'twitter': '',
                    'facebook': ''
                },
                'picture': 'https://lh3.googleusercontent.com/-zsTF7OpE0IY/AAAAAAAAAAI/AAAAAAAAATY/oSaQNizpOp8/photo.jpg?sz=320'
            }
        ];

        this.getTec = getTec;
        this.getBlog = getBlog;
        this.getPortfolio = getPortfolio;

        function getTec() {
            return team;
        }

        function getBlog() {
            return $http.get('../../../data/blog.json');
        }

        function getPortfolio() {
            return $http.get('../../../data/portfolio.json');
        }

        function getTools() {
            return $http.get('../../../data/tools.json');
        }

    }

})();
