(function() {
  'use strict';

  angular
    .module('fuzenetv1')
    .directive('acmeNavbar', acmeNavbar);

  /** @ngInject */
  function acmeNavbar() {
    var directive = {
      restrict: 'E',
      templateUrl: 'app/components/navbar/navbar.html',
      scope: {
          creationDate: '='
      },
      controller: NavbarController,
      controllerAs: 'gq',
      bindToController: true
    };

    return directive;

    /** @ngInject */
    function NavbarController(moment, $http) {
        $http.defaults.useXDomain = true;
        $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";

      var gq = this;

      // "gq.creation" is avaible by directive option "bindToController: true"
      gq.relativeDate = moment(gq.creationDate).fromNow();

      gq.quoteData = {
        qemail: '',
        createdq_at: ''
      };

      gq.quoteSubmit = function() {
        gq.quoteData.createdq_at = new Date();
        $http({
          method  : 'POST',
          url     : 'http://fuzenet.local/src/backend/getquote.php',
          data    : $.param(gq.quoteData),  // pass in data as strings
          headers : { 'Content-Type': 'application/x-www-form-urlencoded' } // set the headers so angular passing info as form data (not request payload)
        })
        .success(function(data) {
          if (!data.success) {
            toastr.error(data.message);
          } else {
            //Before erase the formData make a copy of all the fields in the contact.json
            gq.quoteData = {};
            toastr.success(data.message);
          }
        });

        return false;
      }
    }
  }

})();
