(function () {
    'use strict';

    angular
        .module('fuzenetv1')
        .controller('PortfolioController', PortfolioController);

    /** @ngInject */
    function PortfolioController($stateParams, webDevTec, $filter) {
        var vm = this;
        vm.category = $stateParams['category'];
        vm.categoryFilter = '';
        vm.subcategoryFilter = '';
        vm.portfolio = [];

        activate();

        function activate() {

            getPortfolio();
        }

        function getPortfolio() {
            return webDevTec.getPortfolio().then(function (data) {
                vm.portfolio = data.data;
            });
        }
    }
})();
