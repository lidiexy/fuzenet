(function () {
    'use strict';

    angular
        .module('fuzenetv1')
        .controller('BlogController', BlogController);

    /** @ngInject */
    function BlogController(webDevTec, $filter) {
        var vm = this;
        vm.blog = [];
        vm.team = [];
        vm.searchcrit = '';

        activate();

        function activate() {
            var wow = new WOW(
                {
                    boxClass: 'wow',      // animated element css class (default is wow)
                    animateClass: 'animated', // animation css class (default is animated)
                    offset: 0,          // distance to the element when triggering the animation (default is 0)
                    mobile: true,       // trigger animations on mobile devices (default is true)
                    live: true,       // act on asynchronously loaded content (default is true)
                    callback: function (box) {
                        // the callback is fired every time an animation is started
                        // the argument that is passed in is the DOM node being animated
                    }
                }
            );
            wow.init();

            vm.team = getOwners();
            getServiceBlog();
        }

        function getServiceBlog() {
            return webDevTec.getBlog().then(function (data) {
                vm.blog = data.data;
                angular.forEach(vm.blog, function(post){
                    var who = $filter('filter')(vm.team, {name: post.author})[0];
                    post.ownerpic = who.picture;
                });
            });
        }

        function getOwners() {
            return webDevTec.getTec();
        }

        vm.searchFn = function (item){
            if (item.title.indexOf(vm.searchcrit)!=-1 || item.summary.indexOf(vm.searchcrit)!=-1) {
                return true;
            }
            return false;
        };

    }
})();
