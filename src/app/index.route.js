(function () {
    'use strict';

    angular
        .module('fuzenetv1')
        .config(routeConfig);

    /** @ngInject */
    function routeConfig($stateProvider, $urlRouterProvider, $locationProvider) {
        $stateProvider
            .state('home', {
                url: '/',
                templateUrl: 'app/main/main.html',
                controller: 'MainController',
                controllerAs: 'main'
            })
            .state('blog', {
                url: '/blog',
                templateUrl: 'app/blog/blog.html',
                controller: 'BlogController',
                controllerAs: 'vm'
            })
            .state('blogpost', {
                url: '/blog/:id',
                templateUrl: 'app/blogpost/blogpost.html',
                controller: 'BlogpostController',
                controllerAs: 'vm'
            })
            .state('articles', {
                url: '/articles',
                templateUrl: 'app/articles/articles.html',
                controller: 'ArticlesController',
                controllerAs: 'vm'
            })
            .state('articlepost', {
                url: '/articles/:id',
                templateUrl: 'app/articlepost/articlepost.html',
                controller: 'ArticlepostController',
                controllerAs: 'vm'
            })
            .state('tools', {
                url: '/tools',
                templateUrl: 'app/tools/tools.html',
                controller: 'ToolsController',
                controllerAs: 'vm'
            })
            .state('toolspost', {
                url: '/tools/:id',
                templateUrl: 'app/toolspost/toolspost.html',
                controller: 'ToolpostController',
                controllerAs: 'vm'
            })
            .state('portfolio', {
                url: '/portfolio',
                templateUrl: 'app/portfolio/portfolio.html',
                controller: 'PortfolioController',
                params : {
                    category: null
                },
                controllerAs: 'vm'
            })
            .state('about', {
                url: '/about',
                templateUrl: 'app/about/about.html',
                controller: 'AboutController',
                controllerAs: 'vm'
            });

        $urlRouterProvider.otherwise('/');
        /*$locationProvider.html5Mode(true).hashPrefix('!');*/ //Para eliminar las URL con /#/
    }

})();
