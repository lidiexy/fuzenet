(function() {
  'use strict';

  angular
    .module('fuzenetv1')
    .controller('MainController', ['$timeout','webDevTec', 'toastr', '$http', MainController]);

  /** @ngInject */
  function MainController($timeout, webDevTec, toastr) {
    var vm = this;

    vm.slides = [];
    vm.team = [];
    vm.portfolio = [];
    vm.category = 'design';
    vm.categoryFilter = 'design';
    vm.subcategoryFilter = '';
    vm.blog = [];
    vm.classAnimation = '';
    vm.creationDate = 1438102123609;
    vm.showToastr = showToastr;

    activate();

    function activate() {
      vm.slides = [
        {
          image:'../assets/images/slidermain/1.png',
          title:'Titulo 1',
          tagline:'Tagline asociado 1'
        },{
          image:'../assets/images/slidermain/2.png',
          title:'Titulo 2',
          tagline:'Tagline asociado 2',
        },{
          image:'../assets/images/slidermain/3.png',
          title:'Titulo 3',
          tagline:'Tagline asociado 3'
        }
      ];
      vm.team = getTeamTec();
      getPortfolio();
      getServiceBlog();
      $timeout(function() {
        vm.classAnimation = 'bounceInDown';
      }, 4000);
    }

    function showToastr() {
      toastr.info('Fork <a href="https://github.com/Swiip/generator-gulp-angular" target="_blank"><b>generator-gulp-angular</b></a>');
      vm.classAnimation = '';
    }

    function getTeamTec() {
       return webDevTec.getTec();
      /*angular.forEach(vm.awesomeThings, function(awesomeThing) {
        awesomeThing.rank = Math.random();
      });*/
    }

    function getServiceBlog() {
      return webDevTec.getBlog().then(function(data){
        vm.blog = data.data;
      });
    }

    function getPortfolio() {
      return webDevTec.getPortfolio().then(function(data){
        vm.portfolio = data.data;
      });
    }

  }
})();
