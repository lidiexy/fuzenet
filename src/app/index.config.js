(function() {
  'use strict';

  angular
    .module('fuzenetv1')
    .config(config);

  /** @ngInject */
  function config($logProvider, toastr, $httpProvider) {
    // Enable log
    $logProvider.debugEnabled(true);

    // Set options third-party lib
    toastr.options.timeOut = 3000;
    toastr.options.positionClass = 'toast-top-right';
    toastr.options.preventDuplicates = true;
    toastr.options.progressBar = true;

    delete $httpProvider.defaults.headers.common['X-Requested-With'];

  }

})();
