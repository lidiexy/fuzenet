(function () {
    'use strict';

    angular
        .module('fuzenetv1')
        .controller('BlogpostController', BlogpostController);

    /** @ngInject */
    function BlogpostController($stateParams, webDevTec, $filter, $http, toastr) {
        var vm = this;
        vm.postlist = [];
        vm.post = {}
        vm.team = [];
        vm.postid = $stateParams['id'];

        activate();

        function activate() {
            vm.team = getOwners();
            getPost();
        }

        function getPost() {
            webDevTec.getBlog().then(function(data){
                vm.postlist = data.data;
                vm.post = $filter('filter')(vm.postlist, {id: vm.postid})[0];
                var who = $filter('filter')(vm.team, {name: vm.post.author})[0];
                vm.post.who = who;
            });
        }

        function getOwners() {
            return webDevTec.getTec();
        }

        vm.commentData = {
            postid: vm.postid,
            author: '',
            body: '',
            creationdate: '',
        };

        vm.commentSubmit = function() {
            vm.commentData.postid = vm.postid;
            $http({
                method  : 'POST',
                url     : 'http://fuzenet.local/src/backend/addblogcomment.php',
                data    : $.param(vm.commentData),
                headers : { 'Content-Type': 'application/x-www-form-urlencoded' } // set the headers so angular passing info as form data (not request payload)
            })
                .success(function(data) {
                    if (!data.success) {
                        toastr.error(data.message);
                    } else {
                        //Before erase the formData make a copy of all the fields in the contact.json
                        vm.commentData = {
                            postid: vm.postid,
                            author: '',
                            body: '',
                            creationdate: '',
                        };
                        toastr.success(data.message);
                    }
                });
            return false;
        }
    }
})();
