(function() {
  'use strict';

  angular
    .module('fuzenetv1')
    .run(runBlock);

  /** @ngInject */
  function runBlock($log) {

    $log.debug('runBlock end');
  }

})();
