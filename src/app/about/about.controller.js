(function() {
  'use strict';

  angular
    .module('fuzenetv1')
    .controller('AboutController', AboutController);

  /** @ngInject */
  function AboutController($timeout, webDevTec, toastr) {
    var vm = this;

    vm.team = [];
    vm.classAnimation = '';
    vm.showToastr = showToastr;

    activate();

    function activate() {
      vm.team = getWebDevTec();
      $timeout(function() {
        vm.classAnimation = 'bounceInDown';
      }, 4000);
    }

    function showToastr() {
      toastr.info('<strong>Cualquier HTML Aqui</strong>');
      vm.classAnimation = '';
    }

    function getWebDevTec() {
       return webDevTec.getTec();
    }
  }
})();
